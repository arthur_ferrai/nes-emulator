from src.device import Device


class RAM(Device):
    def __init__(self):
        self.data = {}

    def get(self, address: int) -> int:
        return self.data.get(address, 0)

    def set(self, address: int, value: int) -> int:
        self.data[address] = value
        return self.data[address]

    def __setitem__(self, address: int, value: int) -> int:
        return self.set(address, value)

    def __getitem__(self, address: int) -> int:
        return self.get(address)
