from assertpy import assert_that

from test.cpu.utils import CpuTestUtils


class TestCpuStartup(CpuTestUtils):
    def test_on_start_cpu_registers_must_be_correct(self):
        assert_that(self.cpu).has_no_changes()
