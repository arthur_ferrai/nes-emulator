class _INSTR:
    __slots__ = ()


"""
Addressing modes:
Immediate:
The next word is the value to use

Zero Page:
The next byte is the zero-page address of the value to use

Zero Page, X:
Same as "Zero Page", but adds X to the address (wraps around if sum > 0xFF)

Zero Page, Y:
Same as "Zero Page, X", but uses Y

Relative:
The next byte is added to PC

Absolute:
The next word is the address of the value to use

Absolute, X:
Same as "Absolute", but adds X to the address

Absolute, Y:
Same as "Absolute, X", but uses Y

Indirect:
The next word is an address of the address of the value

Indirect, X:
Joins "Indirect" and "Zero Page, X": the next byte plus X is a zero-page address of the address of the value

Indirect, Y
The next byte is a zero-page address. This value plus Y is the address of value
"""


# Load/Store operations
class LDA(_INSTR):
    IMMEDIATE = 0xA9
    ZERO_PAGE = 0xA5
    ZERO_PAGE_X = 0xB5
    ABSOLUTE = 0xAD
    ABSOLUTE_X = 0xBD
    ABSOLUTE_Y = 0xB9
    INDIRECT_X = 0xA1
    INDIRECT_Y = 0xB1


class LDX(_INSTR):
    IMMEDIATE = 0xA2
    ZERO_PAGE = 0xA6
    ZERO_PAGE_Y = 0xB6
    ABSOLUTE = 0xAE
    ABSOLUTE_Y = 0xBE


class LDY(_INSTR):
    IMMEDIATE = 0xA0
    ZERO_PAGE = 0xA4
    ZERO_PAGE_X = 0xB4
    ABSOLUTE = 0xAC
    ABSOLUTE_X = 0xBC


class STA(_INSTR):
    ZERO_PAGE = 0x85
    ZERO_PAGE_X = 0x95
    ABSOLUTE = 0x8D
    ABSOLUTE_X = 0x9D
    ABSOLUTE_Y = 0x99
    INDIRECT_X = 0x81
    INDIRECT_Y = 0x91


class STX(_INSTR):
    ZERO_PAGE = 0x86
    ZERO_PAGE_Y = 0x96
    ABSOLUTE = 0x8E


class STY(_INSTR):
    ZERO_PAGE = 0x84
    ZERO_PAGE_X = 0x94
    ABSOLUTE = 0x8C


# Register transfer
class TAX(_INSTR):
    IMPLIED = 0xAA


class TAY(_INSTR):
    IMPLIED = 0xA8


class TXA(_INSTR):
    IMPLIED = 0x8A


class TYA(_INSTR):
    IMPLIED = 0x98


# Stack Operation
class TSX(_INSTR):
    IMPLIED = 0xBA


class TXS(_INSTR):
    IMPLIED = 0x9A


class PHA(_INSTR):
    IMPLIED = 0x48


class PHP(_INSTR):
    IMPLIED = 0x08


class PLA(_INSTR):
    IMPLIED = 0x68


class PLP(_INSTR):
    IMPLIED = 0x28


# Logical
class AND(_INSTR):
    IMMEDIATE = 0x29
    ZERO_PAGE = 0x25
    ZERO_PAGE_X = 0x35
    ABSOLUTE = 0x2D
    ABSOLUTE_X = 0x3D
    ABSOLUTE_Y = 0x39
    INDIRECT_X = 0x21
    INDIRECT_Y = 0x31


class EOR(_INSTR):
    IMMEDIATE = 0x49
    ZERO_PAGE = 0x45
    ZERO_PAGE_X = 0x55
    ABSOLUTE = 0x4D
    ABSOLUTE_X = 0x5D
    ABSOLUTE_Y = 0x59
    INDIRECT_X = 0x41
    INDIRECT_Y = 0x51


class ORA(_INSTR):
    IMMEDIATE = 0x09
    ZERO_PAGE = 0x05
    ZERO_PAGE_X = 0x15
    ABSOLUTE = 0x0D
    ABSOLUTE_X = 0x1D
    ABSOLUTE_Y = 0x19
    INDIRECT_X = 0x01
    INDIRECT_Y = 0x11


class BIT(_INSTR):
    ZERO_PAGE = 0x24
    ABSOLUTE = 0x2C


# Arithmetic
class ADC(_INSTR):
    IMMEDIATE = 0x69
    ZERO_PAGE = 0x65
    ZERO_PAGE_X = 0x75
    ABSOLUTE = 0x6D
    ABSOLUTE_X = 0x7D
    ABSOLUTE_Y = 0x79
    INDIRECT_X = 0x61
    INDIRECT_Y = 0x71


class SBC(_INSTR):
    IMMEDIATE = 0xE9
    ZERO_PAGE = 0xE5
    ZERO_PAGE_X = 0xF5
    ABSOLUTE = 0xED
    ABSOLUTE_X = 0xFD
    ABSOLUTE_Y = 0xF9
    INDIRECT_X = 0xE1
    INDIRECT_Y = 0xF1


class CMP(_INSTR):
    IMMEDIATE = 0xC9
    ZERO_PAGE = 0xC5
    ZERO_PAGE_X = 0xD5
    ABSOLUTE = 0xCD
    ABSOLUTE_X = 0xDD
    ABSOLUTE_Y = 0xD9
    INDIRECT_X = 0xC1
    INDIRECT_Y = 0xD1


class CPX(_INSTR):
    IMMEDIATE = 0xE0
    ZERO_PAGE = 0xE4
    ABSOLUTE = 0xEC


class CPY(_INSTR):
    IMMEDIATE = 0xC0
    ZERO_PAGE = 0xC4
    ABSOLUTE = 0xCC


# Increment/Decrement
class INC(_INSTR):
    ZERO_PAGE = 0xE6
    ZERO_PAGE_X = 0xF6
    ABSOLUTE = 0xEE
    ABSOLUTE_X = 0xFE


class INX(_INSTR):
    IMPLIED = 0xE8


class INY(_INSTR):
    IMPLIED = 0xC8


class DEC(_INSTR):
    ZERO_PAGE = 0xC6
    ZERO_PAGE_X = 0xD6
    ABSOLUTE = 0xCE
    ABSOLUTE_X = 0xDE


class DEX(_INSTR):
    IMPLIED = 0xCA


class DEY(_INSTR):
    IMPLIED = 0x88


# Shifts
# missing: LSR, ROL, ROR
class ASL(_INSTR):
    ACCUMULATOR = 0x0A
    ZERO_PAGE = 0x06
    ZERO_PAGE_X = 0x16
    ABSOLUTE = 0x0E
    ABSOLUTE_X = 0x1E


# Jump/Call
class JMP(_INSTR):
    ABSOLUTE = 0x4C
    INDIRECT = 0x6C


class JSR(_INSTR):
    ABSOLUTE = 0x20


class RTS(_INSTR):
    IMPLIED = 0x60


# Branches
# missing:all


# Status flag changes
# missing:all

# System functions
# missing:all
