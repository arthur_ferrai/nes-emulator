from src.cpu import instructions
from src.bus import Bus


def _get_bit(v, shift):
    return (v >> shift) & 1


def _wrap_stack(value):
    if value >= 0:
        return value & 0xFF
    return 0x100 + value


def _compose_word(lower, upper):
    return lower | (upper << 8)


def _consume_clock(amount=1):
    for _ in range(amount):
        yield


def _add_bytes(left_byte, right_byte):
    yield from _consume_clock()
    return (left_byte + right_byte) & 0xFF


def _add_words(left_word, right_word):
    yield from _consume_clock()
    return left_word + right_word & 0xFFFF


def _get_sign(value):
    return bool(value & (1 << 7))


class CPU(object):
    def __init__(self, bus: Bus):
        self.bus = bus
        self.A = 0x0    # accumulator, 8-bit
        self.X = 0x0    # X register, 8-bit
        self.Y = 0x0    # Y register, 8-bit
        self.SP = 0xFD  # Stack Pointer, 8-bit
        self.PC = _compose_word(self.bus.read(0xFFFC), self.bus.read(0xFFFD))  # Program Counter, 16-bit

        # status flags (initial values)
        self.C = False  # Carry
        self.Z = False  # Zero
        self.I = True   # Disable IRQ
        self.D = False  # Decimal mode (unused)
        self.B = True   # Break (interruption)
        self.V = False  # Overflow
        self.N = False  # Negative

        self._execution = self._execute()
        next(self._execution)

    def reset(self):
        self.PC = _compose_word(self.bus.read(0xFFFC), self.bus.read(0xFFFD))
        self.SP = _wrap_stack(self.SP - 3)
        self.I = True

    @property
    def P(self):
        return int(self.C) | \
               (int(self.Z) << 1) | \
               (int(self.I) << 2) | \
               (int(self.D) << 3) | \
               (int(self.B) << 4) | \
               (1 << 5) | \
               (int(self.V) << 6) | \
               (int(self.N) << 7)

    @P.setter
    def P(self, v):
        self.C = bool(_get_bit(v, 0))
        self.Z = bool(_get_bit(v, 1))
        self.I = bool(_get_bit(v, 2))
        self.D = bool(_get_bit(v, 3))
        self.B = bool(_get_bit(v, 4))
        self.V = bool(_get_bit(v, 6))
        self.N = bool(_get_bit(v, 7))

    def clock(self):
        next(self._execution)

    def _fetch_byte_from_PC(self):
        data = yield from self._read_byte(self.PC)
        self.PC += 1
        return data

    def _fetch_word_from_PC(self):
        lower_addr = yield from self._fetch_byte_from_PC()
        upper_addr = yield from self._fetch_byte_from_PC()
        return _compose_word(lower_addr, upper_addr)

    def _fetch_word_from_PC_with_boundary_check(self, value_to_add):
        lower_addr = yield from self._fetch_byte_from_PC()
        upper_addr = yield from self._fetch_byte_from_PC()
        lower_addr += value_to_add
        if lower_addr > 0xFF:
            yield from _consume_clock()
        return _compose_word(lower_addr, upper_addr)

    def _fetch_absolute_from_memory_with_boundary_check(self, offset):
        addr = yield from self._fetch_word_from_PC_with_boundary_check(offset)
        data = yield from self._read_byte(addr)
        return data

    def _set_Z_N(self, data):
        self.Z = data == 0
        self.N = bool(data & (1 << 7))

    def _set_A_Z_N(self, data):
        self.A = data
        self._set_Z_N(data)

    def _set_X_Z_N(self, data):
        self.X = data
        self._set_Z_N(data)

    def _set_Y_Z_N(self, data):
        self.Y = data
        self._set_Z_N(data)

    def _execute(self):
        while True:
            instruction = yield from self._fetch_byte_from_PC()
            if instruction == instructions.LDA.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._set_A_Z_N(data)

            elif instruction == instructions.LDA.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._set_A_Z_N(data)

            elif instruction == instructions.LDA.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._set_A_Z_N(data)

            elif instruction == instructions.LDA.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._set_A_Z_N(data)

            elif instruction == instructions.LDA.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._set_A_Z_N(data)

            elif instruction == instructions.LDA.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._set_A_Z_N(data)

            elif instruction == instructions.LDA.INDIRECT_X:
                data = yield from self._fetch_indirect_X_from_memory()
                self._set_A_Z_N(data)

            elif instruction == instructions.LDA.INDIRECT_Y:
                data = yield from self._fetch_indirect_Y_from_memory()
                self._set_A_Z_N(data)

            elif instruction == instructions.JMP.ABSOLUTE:
                self.PC = yield from self._fetch_word_from_PC()

            elif instruction == instructions.JMP.INDIRECT:
                ind_addr = yield from self._fetch_word_from_PC()
                self.PC = yield from self._read_word(ind_addr)

            elif instruction == instructions.LDX.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._set_X_Z_N(data)

            elif instruction == instructions.LDX.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._set_X_Z_N(data)

            elif instruction == instructions.LDX.ZERO_PAGE_Y:
                addr = yield from self._fetch_byte_from_PC()
                addr = yield from _add_bytes(addr, self.Y)
                data = yield from self._read_byte(addr)
                self._set_X_Z_N(data)

            elif instruction == instructions.LDX.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._set_X_Z_N(data)

            elif instruction == instructions.LDX.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._set_X_Z_N(data)

            elif instruction == instructions.LDY.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._set_Y_Z_N(data)

            elif instruction == instructions.LDY.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._set_Y_Z_N(data)

            elif instruction == instructions.LDY.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._set_Y_Z_N(data)

            elif instruction == instructions.LDY.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._set_Y_Z_N(data)

            elif instruction == instructions.LDY.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._set_Y_Z_N(data)

            elif instruction == instructions.STA.ZERO_PAGE:
                addr = yield from self._fetch_byte_from_PC()
                yield from self._write_byte(addr, self.A)

            elif instruction == instructions.STA.ZERO_PAGE_X:
                addr = yield from self._fetch_zero_page_with_X_from_PC()
                yield from self._write_byte(addr, self.A)

            elif instruction == instructions.STA.ABSOLUTE:
                addr = yield from self._fetch_word_from_PC()
                yield from self._write_byte(addr, self.A)

            elif instruction == instructions.STA.ABSOLUTE_X:
                addr = yield from self._fetch_word_from_PC()
                addr = yield from _add_words(addr, self.X)
                yield from self._write_byte(addr, self.A)

            elif instruction == instructions.STA.ABSOLUTE_Y:
                addr = yield from self._fetch_word_from_PC()
                addr = yield from _add_words(addr, self.Y)
                yield from self._write_byte(addr, self.A)

            elif instruction == instructions.STA.INDIRECT_X:
                addr = yield from self._fetch_indirect_X_address_from_PC()
                yield from self._write_byte(addr, self.A)

            elif instruction == instructions.STA.INDIRECT_Y:
                addr = yield from self._fetch_indirect_Y_address_from_PC(True)
                yield from self._write_byte(addr, self.A)

            elif instruction == instructions.STX.ZERO_PAGE:
                addr = yield from self._fetch_byte_from_PC()
                yield from self._write_byte(addr, self.X)

            elif instruction == instructions.STX.ZERO_PAGE_Y:
                addr = yield from self._fetch_byte_from_PC()
                addr = yield from _add_bytes(addr, self.Y)
                yield from self._write_byte(addr, self.X)

            elif instruction == instructions.STX.ABSOLUTE:
                addr = yield from self._fetch_word_from_PC()
                yield from self._write_byte(addr, self.X)

            elif instruction == instructions.STY.ZERO_PAGE:
                addr = yield from self._fetch_byte_from_PC()
                yield from self._write_byte(addr, self.Y)

            elif instruction == instructions.STY.ZERO_PAGE_X:
                addr = yield from self._fetch_zero_page_with_X_from_PC()
                yield from self._write_byte(addr, self.Y)

            elif instruction == instructions.STY.ABSOLUTE:
                addr = yield from self._fetch_word_from_PC()
                yield from self._write_byte(addr, self.Y)

            elif instruction == instructions.JSR.ABSOLUTE:
                addr = yield from self._fetch_word_from_PC()
                yield from self._push_word_to_stack(self.PC - 1)
                yield from _consume_clock()
                self.PC = addr

            elif instruction == instructions.RTS.IMPLIED:
                addr = yield from self._pop_word_from_stack()
                yield from _consume_clock(3)
                self.PC = addr + 1

            elif instruction == instructions.TAX.IMPLIED:
                yield from _consume_clock()
                self._set_X_Z_N(self.A)

            elif instruction == instructions.TAY.IMPLIED:
                yield from _consume_clock()
                self._set_Y_Z_N(self.A)

            elif instruction == instructions.TSX.IMPLIED:
                yield from _consume_clock()
                self._set_X_Z_N(self.SP)

            elif instruction == instructions.TXA.IMPLIED:
                yield from _consume_clock()
                self._set_A_Z_N(self.X)

            elif instruction == instructions.TXS.IMPLIED:
                yield from _consume_clock()
                self.SP = self.X

            elif instruction == instructions.TYA.IMPLIED:
                yield from _consume_clock()
                self._set_A_Z_N(self.Y)

            elif instruction == instructions.PHA.IMPLIED:
                yield from _consume_clock()
                yield from self._push_to_stack(self.A)

            elif instruction == instructions.PHP.IMPLIED:
                yield from _consume_clock()
                yield from self._push_to_stack(self.P)

            elif instruction == instructions.PLA.IMPLIED:
                data = yield from self._pop_from_stack()
                yield from _consume_clock(2)
                self._set_A_Z_N(data)

            elif instruction == instructions.PLP.IMPLIED:
                data = yield from self._pop_from_stack()
                yield from _consume_clock(2)
                self.P = data

            elif instruction == instructions.ADC.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.ADC.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.ADC.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.ADC.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.ADC.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.ADC.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.ADC.INDIRECT_X:
                data = yield from self._fetch_indirect_X_from_memory()
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.ADC.INDIRECT_Y:
                data = yield from self._fetch_indirect_Y_from_memory()
                self._sum_with_zero_carry_negative_and_overflow(data)

            elif instruction == instructions.SBC.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.SBC.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.SBC.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.SBC.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.SBC.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.SBC.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.SBC.INDIRECT_X:
                data = yield from self._fetch_indirect_X_from_memory()
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.SBC.INDIRECT_Y:
                data = yield from self._fetch_indirect_Y_from_memory()
                self._sub_with_C_Z_V_N_flags(data)

            elif instruction == instructions.CMP.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CMP.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CMP.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CMP.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CMP.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CMP.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CMP.INDIRECT_X:
                data = yield from self._fetch_indirect_X_from_memory()
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CMP.INDIRECT_Y:
                data = yield from self._fetch_indirect_Y_from_memory()
                self._compare_and_set_C_Z_N(self.A, data)

            elif instruction == instructions.CPX.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._compare_and_set_C_Z_N(self.X, data)

            elif instruction == instructions.CPX.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._compare_and_set_C_Z_N(self.X, data)

            elif instruction == instructions.CPX.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._compare_and_set_C_Z_N(self.X, data)

            elif instruction == instructions.CPY.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._compare_and_set_C_Z_N(self.Y, data)

            elif instruction == instructions.CPY.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._compare_and_set_C_Z_N(self.Y, data)

            elif instruction == instructions.CPY.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._compare_and_set_C_Z_N(self.Y, data)

            elif instruction == instructions.AND.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._AND_operation(data)

            elif instruction == instructions.AND.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._AND_operation(data)

            elif instruction == instructions.AND.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._AND_operation(data)

            elif instruction == instructions.AND.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._AND_operation(data)

            elif instruction == instructions.AND.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._AND_operation(data)

            elif instruction == instructions.AND.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._AND_operation(data)

            elif instruction == instructions.AND.INDIRECT_X:
                data = yield from self._fetch_indirect_X_from_memory()
                self._AND_operation(data)

            elif instruction == instructions.AND.INDIRECT_Y:
                data = yield from self._fetch_indirect_Y_from_memory()
                self._AND_operation(data)

            elif instruction == instructions.EOR.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._EOR_operation(data)

            elif instruction == instructions.EOR.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._EOR_operation(data)

            elif instruction == instructions.EOR.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._EOR_operation(data)

            elif instruction == instructions.EOR.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._EOR_operation(data)

            elif instruction == instructions.EOR.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._EOR_operation(data)

            elif instruction == instructions.EOR.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._EOR_operation(data)

            elif instruction == instructions.EOR.INDIRECT_X:
                data = yield from self._fetch_indirect_X_from_memory()
                self._EOR_operation(data)

            elif instruction == instructions.EOR.INDIRECT_Y:
                data = yield from self._fetch_indirect_Y_from_memory()
                self._EOR_operation(data)

            elif instruction == instructions.ORA.IMMEDIATE:
                data = yield from self._fetch_byte_from_PC()
                self._ORA_operation(data)

            elif instruction == instructions.ORA.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._ORA_operation(data)

            elif instruction == instructions.ORA.ZERO_PAGE_X:
                data = yield from self._fetch_zero_page_with_X_from_memory()
                self._ORA_operation(data)

            elif instruction == instructions.ORA.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._ORA_operation(data)

            elif instruction == instructions.ORA.ABSOLUTE_X:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.X)
                self._ORA_operation(data)

            elif instruction == instructions.ORA.ABSOLUTE_Y:
                data = yield from self._fetch_absolute_from_memory_with_boundary_check(self.Y)
                self._ORA_operation(data)

            elif instruction == instructions.ORA.INDIRECT_X:
                data = yield from self._fetch_indirect_X_from_memory()
                self._ORA_operation(data)

            elif instruction == instructions.ORA.INDIRECT_Y:
                data = yield from self._fetch_indirect_Y_from_memory()
                self._ORA_operation(data)

            elif instruction == instructions.BIT.ZERO_PAGE:
                data = yield from self._fetch_zero_page_memory()
                self._BIT_operation(data)

            elif instruction == instructions.BIT.ABSOLUTE:
                data = yield from self._fetch_absolute_byte()
                self._BIT_operation(data)

            elif instruction == instructions.INC.ZERO_PAGE:
                addr = yield from self._fetch_byte_from_PC()
                yield from self._INC_operation(addr)

            elif instruction == instructions.INC.ZERO_PAGE_X:
                addr = yield from self._fetch_zero_page_with_X_from_PC()
                yield from self._INC_operation(addr)

            elif instruction == instructions.INC.ABSOLUTE:
                addr = yield from self._fetch_word_from_PC()
                yield from self._INC_operation(addr)

            elif instruction == instructions.INC.ABSOLUTE_X:
                addr = yield from self._fetch_word_from_PC()
                addr = yield from _add_words(addr, self.X)
                yield from self._INC_operation(addr)

            elif instruction == instructions.INX.IMPLIED:
                self.X = yield from _add_bytes(self.X, 1)
                self._set_Z_N(self.X)

            elif instruction == instructions.INY.IMPLIED:
                self.Y = yield from _add_bytes(self.Y, 1)
                self._set_Z_N(self.Y)

            elif instruction == instructions.DEC.ZERO_PAGE:
                addr = yield from self._fetch_byte_from_PC()
                yield from self._DEC_operation(addr)

            elif instruction == instructions.DEC.ZERO_PAGE_X:
                addr = yield from self._fetch_zero_page_with_X_from_PC()
                yield from self._DEC_operation(addr)

            elif instruction == instructions.DEC.ABSOLUTE:
                addr = yield from self._fetch_word_from_PC()
                yield from self._DEC_operation(addr)

            elif instruction == instructions.DEC.ABSOLUTE_X:
                addr = yield from self._fetch_word_from_PC()
                addr = yield from _add_words(addr, self.X)
                yield from self._DEC_operation(addr)

            elif instruction == instructions.DEX.IMPLIED:
                self.X = yield from _add_bytes(self.X, -1)
                self._set_Z_N(self.X)

            elif instruction == instructions.DEY.IMPLIED:
                self.Y = yield from _add_bytes(self.Y, -1)
                self._set_Z_N(self.Y)

            elif instruction == instructions.ASL.ACCUMULATOR:
                yield from _consume_clock()
                self.A = self._ASL_operation(self.A)

            elif instruction == instructions.ASL.ZERO_PAGE:
                yield from _consume_clock()
                addr = yield from self._fetch_byte_from_PC()
                value = yield from self._read_byte(addr)
                yield from self._write_byte(addr, self._ASL_operation(value))

            elif instruction == instructions.ASL.ZERO_PAGE_X:
                yield from _consume_clock()
                addr = yield from self._fetch_zero_page_with_X_from_PC()
                value = yield from self._read_byte(addr)
                yield from self._write_byte(addr, self._ASL_operation(value))

            elif instruction == instructions.ASL.ABSOLUTE:
                yield from _consume_clock()
                addr = yield from self._fetch_word_from_PC()
                value = yield from self._read_byte(addr)
                yield from self._write_byte(addr, self._ASL_operation(value))

            elif instruction == instructions.ASL.ABSOLUTE_X:
                yield from _consume_clock()
                addr = yield from self._fetch_word_from_PC()
                addr = yield from _add_words(addr, self.X)
                value = yield from self._read_byte(addr)
                yield from self._write_byte(addr, self._ASL_operation(value))

    def _ASL_operation(self, value):
        result = value << 1
        self.C = bool(_get_bit(result, 8))
        ret = result & 0xFF
        self._set_Z_N(ret)
        return ret

    def _INC_operation(self, addr):
        data = yield from self._read_byte(addr)
        new_data = yield from _add_bytes(data, 1)
        yield from self._write_byte(addr, new_data)
        self._set_Z_N(new_data)

    def _DEC_operation(self, addr):
        data = yield from self._read_byte(addr)
        new_data = yield from _add_bytes(data, -1)
        yield from self._write_byte(addr, new_data)
        self._set_Z_N(new_data)

    def _BIT_operation(self, data):
        self.Z = not bool(self.A & data)
        self.N = bool(_get_bit(data, 7))
        self.V = bool(_get_bit(data, 6))

    def _AND_operation(self, data):
        self.A &= data
        self._set_Z_N(self.A)

    def _EOR_operation(self, data):
        self.A ^= data
        self._set_Z_N(self.A)

    def _ORA_operation(self, data):
        self.A |= data
        self._set_Z_N(self.A)

    def _fetch_indirect_Y_from_memory(self):
        addr = yield from self._fetch_indirect_Y_address_from_PC()
        data = yield from self._read_byte(addr)
        return data

    def _fetch_indirect_X_from_memory(self):
        addr = yield from self._fetch_indirect_X_address_from_PC()
        data = yield from self._read_byte(addr)
        return data

    def _fetch_zero_page_with_X_from_memory(self):
        addr = yield from self._fetch_zero_page_with_X_from_PC()
        data = yield from self._read_byte(addr)
        return data

    def _fetch_zero_page_memory(self):
        addr = yield from self._fetch_byte_from_PC()
        data = yield from self._read_byte(addr)
        return data

    def _fetch_absolute_byte(self):
        addr = yield from self._fetch_word_from_PC()
        data = yield from self._read_byte(addr)
        return data

    def _compare_and_set_C_Z_N(self, left, right):
        sub = left + (0xFF - right) + 1  # using one's complement
        self.C = bool(_get_bit(sub, 8))
        self._set_Z_N(sub & 0xFF)

    def _fetch_indirect_Y_address_from_PC(self, force_extra_cycle=False):
        ind_addr = yield from self._fetch_byte_from_PC()
        lower_addr = yield from self._read_byte(ind_addr)
        upper_addr = yield from self._read_byte(ind_addr + 1)
        lower_addr += self.Y
        if force_extra_cycle or lower_addr > 0xFF:
            yield from _consume_clock()
        addr = _compose_word(lower_addr, upper_addr)
        return addr

    def _sub_with_C_Z_V_N_flags(self, data):
        data_compl = 0xFF - data  # using one's complement
        sub = self.A + data_compl + int(self.C)
        self._set_V_on_sum(self.A, data_compl, sub)
        self.C = bool(_get_bit(sub, 8))
        self.A = sub & 0xFF  # make sure it has one byte
        self._set_Z_N(self.A)

    def _fetch_indirect_X_address_from_PC(self):
        ind_addr = yield from self._fetch_zero_page_with_X_from_PC()
        addr = yield from self._read_word(ind_addr)
        return addr

    def _sum_with_zero_carry_negative_and_overflow(self, data):
        add = data + self.A + int(self.C)
        self.C = add > 0xFF
        self._set_V_on_sum(self.A, data, add)
        self.A = add & 0xFF
        self.N = bool(_get_bit(self.A, 7))
        self.Z = not bool(self.A)

    def _fetch_zero_page_with_X_from_PC(self):
        addr = yield from self._fetch_byte_from_PC()
        addr = yield from _add_bytes(addr, self.X)
        return addr

    def _set_V_on_sum(self, left, right, result):
        self.V = ((left_sign := _get_sign(left)) == _get_sign(right)) and (left_sign != _get_sign(result))

    def _read_byte(self, address):
        yield from _consume_clock()
        return self.bus.read(address)

    def _read_word(self, address):
        lower_data = yield from self._read_byte(address)
        upper_data = yield from self._read_byte(address + 1)
        return _compose_word(lower_data, upper_data)

    def _write_byte(self, address, value):
        yield from _consume_clock()
        self.bus.write(address, value)

    def _push_word_to_stack(self, word):
        yield from self._push_to_stack((word & 0xFF00) >> 8)
        yield from self._push_to_stack(word & 0xFF)

    def _push_to_stack(self, byte_to_push):
        yield from self._write_byte(0x100 | self.SP, byte_to_push)
        self.SP = _wrap_stack(self.SP - 1)

    def _pop_word_from_stack(self):
        lower = yield from self._pop_from_stack()
        upper = yield from self._pop_from_stack()
        return _compose_word(lower, upper)

    def _pop_from_stack(self):
        self.SP = _wrap_stack(self.SP + 1)
        data = yield from self._read_byte(0x100 | self.SP)
        return data
