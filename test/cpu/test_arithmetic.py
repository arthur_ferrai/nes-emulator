from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuADCInstructions(CpuTestUtils):
    def test_ADC_immediate(self):
        self.cpu.A = 0x15
        self.ram[0x1000] = instructions.ADC.IMMEDIATE
        self.ram[0x1001] = 0x20

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x35)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_immediate_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.ram[0x1000] = instructions.ADC.IMMEDIATE
        self.ram[0x1001] = 0x20

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x36)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_immediate_set_carry(self):
        self.cpu.A = 0xFF
        self.ram[0x1000] = instructions.ADC.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_immediate_set_overflow(self):
        self.cpu.A = 0x7F
        self.ram[0x1000] = instructions.ADC.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_immediate_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.ADC.IMMEDIATE
        self.ram[0x1001] = 0xFF

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x7F)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_zero_page(self):
        self.cpu.A = 0x15
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x10

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x25)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_zero_page_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x10

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x26)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_zero_page_set_carry(self):
        self.cpu.A = 0xFF
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_zero_page_set_overflow(self):
        self.cpu.A = 0x7F
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_zero_page_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xFF

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x7F)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_zero_page_X(self):
        self.cpu.A = 0x15
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x25).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_zero_page_X_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x26).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_zero_page_X_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x25).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_zero_page_X_set_carry(self):
        self.cpu.A = 0xFF
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_X(0x01)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_zero_page_X_set_overflow(self):
        self.cpu.A = 0x7F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_X(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_zero_page_X_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ZERO_PAGE_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x7F).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_absolute(self):
        self.cpu.A = 0x15
        self.ram[0x1000] = instructions.ADC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_absolute_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.ram[0x1000] = instructions.ADC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1B)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_absolute_set_carry(self):
        self.cpu.A = 0xFF
        self.ram[0x1000] = instructions.ADC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x00)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_absolute_set_overflow(self):
        self.cpu.A = 0x7F
        self.ram[0x1000] = instructions.ADC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x80)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_absolute_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.ADC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x7F)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_ADC_absolute_X(self):
        self.cpu.A = 0x15
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_absolute_X_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x10
        self.ram[0x1100] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_absolute_X_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1B).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_absolute_X_set_carry(self):
        self.cpu.A = 0xFF
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x00).has_X(0x01)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_absolute_X_set_overflow(self):
        self.cpu.A = 0x7F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x80).has_X(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_absolute_X_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x7F).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_absolute_Y(self):
        self.cpu.A = 0x15
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_absolute_Y_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x10
        self.ram[0x1100] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_absolute_Y_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1B).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_absolute_Y_set_carry(self):
        self.cpu.A = 0xFF
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x00).has_Y(0x01)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_absolute_Y_set_overflow(self):
        self.cpu.A = 0x7F
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x80).has_Y(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_absolute_Y_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x7F).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_indirect_X(self):
        self.cpu.A = 0x15
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1A).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_indirect_X_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1A).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_indirect_X_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1B).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_indirect_X_set_carry(self):
        self.cpu.A = 0xFF
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_X(0x01)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_indirect_X_set_overflow(self):
        self.cpu.A = 0x7F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_X(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_indirect_X_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0xFF

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x7F).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_ADC_indirect_Y(self):
        self.cpu.A = 0x15
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05
        self.ram[0x0021] = 0x10
        self.ram[0x1006] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1A).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_indirect_Y_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xFF
        self.ram[0x0021] = 0x10
        self.ram[0x1100] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1A).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_indirect_Y_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05
        self.ram[0x0021] = 0x10
        self.ram[0x1006] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1B).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_indirect_Y_set_carry(self):
        self.cpu.A = 0xFF
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05
        self.ram[0x0021] = 0x10
        self.ram[0x1006] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_Y(0x01)\
            .has_N(False).has_Z(True).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_indirect_Y_set_overflow(self):
        self.cpu.A = 0x7F
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05
        self.ram[0x0021] = 0x10
        self.ram[0x1006] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_Y(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_ADC_indirect_Y_set_carry_and_overflow(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ADC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05
        self.ram[0x0021] = 0x10
        self.ram[0x1006] = 0xFF

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x7F).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))


class TestCpuSBCInstructions(CpuTestUtils):
    def test_SBC_immediate(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.IMMEDIATE
        self.ram[0x1001] = 0x05

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_immediate_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.IMMEDIATE
        self.ram[0x1001] = 0x05

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x0F)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_immediate_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.IMMEDIATE
        self.ram[0x1001] = 0xF0

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_immediate_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.IMMEDIATE
        self.ram[0x1001] = 0x70

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_immediate_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.IMMEDIATE
        self.ram[0x1001] = 0xB0

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0xA0)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_zero_page(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE
        self.ram[0x1001] = 0x15
        self.ram[0x0015] = 0x05

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_zero_page_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x0F)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_zero_page_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xF0

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_zero_page_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x70

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_zero_page_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xB0

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0xA0)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_zero_page_X(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_zero_page_X_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x0F).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_zero_page_X_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.C = False  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x0F).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_zero_page_X_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0xF0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_zero_page_X_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x70

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_zero_page_X_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0xB0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0xA0).has_X(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_absolute(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x10)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_absolute_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x0F)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_absolute_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0xF0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x60)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_absolute_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x70

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x60)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_absolute_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.ram[0x1000] = instructions.SBC.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0xB0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0xA0)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_SBC_absolute_X(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x10).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_absolute_X_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x10
        self.ram[0x1100] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x10).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_absolute_X_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x0F).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_absolute_X_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0xF0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x60).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_absolute_X_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x70

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x60).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_absolute_X_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0xB0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0xA0).has_X(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_absolute_Y(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x10).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_absolute_Y_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x10
        self.ram[0x1100] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x10).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_absolute_Y_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x05

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x0F).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_absolute_Y_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0xF0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x60).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_absolute_Y_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0x70

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x60).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_absolute_Y_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1021] = 0xB0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0xA0).has_Y(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_indirect_X(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_indirect_X_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x05
        self.ram[0x0005] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_indirect_X_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x0F).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_indirect_X_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0xF0

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_indirect_X_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0x70

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60).has_X(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_indirect_X_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True  # flag is inverted on subtract
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_X
        self.ram[0x1001] = 0x20
        self.ram[0x0021] = 0x05
        self.ram[0x0005] = 0xB0

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0xA0).has_X(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_SBC_indirect_Y(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05
        self.ram[0x0021] = 0x10
        self.ram[0x1006] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_indirect_Y_boundary_cross(self):
        self.cpu.A = 0x15
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xFF
        self.ram[0x0021] = 0x10
        self.ram[0x1100] = 0x05

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_indirect_Y_with_carry(self):
        self.cpu.A = 0x15
        self.cpu.C = False
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0x05
        self.ram[0x0021] = 0x10
        self.ram[0x1006] = 0x05

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x0F).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_indirect_Y_set_carry(self):
        self.cpu.A = 0x50
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xFF
        self.ram[0x0021] = 0x10
        self.ram[0x1100] = 0xF0

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(False).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_indirect_Y_set_overflow(self):
        self.cpu.A = 0xD0
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xFF
        self.ram[0x0021] = 0x10
        self.ram[0x1100] = 0x70

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x60).has_Y(0x01)\
            .has_N(False).has_Z(False).has_V(True).has_C(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_SBC_indirect_Y_set_carry_and_overflow(self):
        self.cpu.A = 0x50
        self.cpu.C = True
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.SBC.INDIRECT_Y
        self.ram[0x1001] = 0x20
        self.ram[0x0020] = 0xFF
        self.ram[0x0021] = 0x10
        self.ram[0x1100] = 0xB0

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0xA0).has_Y(0x01)\
            .has_N(True).has_Z(False).has_V(True).has_C(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))


class TestCpuCMPInstructions(CpuTestUtils):
    def test_CMP_immediate_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.ram[0x1000] = instructions.CMP.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_immediate_A_equals_M(self):
        self.cpu.A = 0x1A
        self.ram[0x1000] = instructions.CMP.IMMEDIATE
        self.ram[0x1001] = 0x1A

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_immediate_A_less_than_M(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.CMP.IMMEDIATE
        self.ram[0x1001] = 0xFF

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_immediate_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.ram[0x1000] = instructions.CMP.IMMEDIATE
        self.ram[0x1001] = 0x80

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_immediate_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.ram[0x1000] = instructions.CMP.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_zero_page_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_zero_page_A_equals_M(self):
        self.cpu.A = 0x1A
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x1A

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_zero_page_A_less_than_M(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xFF

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_zero_page_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x80

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_zero_page_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_zero_page_X_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_zero_page_X_boundary_cross(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_zero_page_X_A_equals_M(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_zero_page_X_A_less_than_M(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_zero_page_X_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_zero_page_X_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_absolute_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.ram[0x1000] = instructions.CMP.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_absolute_A_equals_M(self):
        self.cpu.A = 0x1A
        self.ram[0x1000] = instructions.CMP.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_absolute_A_less_than_M(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_absolute_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.ram[0x1000] = instructions.CMP.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_absolute_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.ram[0x1000] = instructions.CMP.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_CMP_absolute_X_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_absolute_X_A_bigger_than_M_boundary_cross(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_absolute_X_A_equals_M(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_absolute_X_A_less_than_M(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_absolute_X_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_absolute_X_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_absolute_Y_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_absolute_Y_A_bigger_than_M_boundary_cross(self):
        self.cpu.A = 0x1A
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_absolute_Y_A_equals_M(self):
        self.cpu.A = 0x1A
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_absolute_Y_A_less_than_M(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_absolute_Y_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_absolute_Y_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2011] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_indirect_X_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x10
        self.ram[0x0012] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_indirect_X_A_bigger_than_M_boundary_cross(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x10
        self.ram[0x0001] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_indirect_X_A_equals_M(self):
        self.cpu.A = 0x1A
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x10
        self.ram[0x0012] = 0x20
        self.ram[0x2010] = 0x1A

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_indirect_X_A_less_than_M(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x10
        self.ram[0x0012] = 0x20
        self.ram[0x2010] = 0xFF

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_indirect_X_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x10
        self.ram[0x0012] = 0x20
        self.ram[0x2010] = 0x80

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_indirect_X_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x10
        self.ram[0x0012] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_CMP_indirect_Y_A_bigger_than_M(self):
        self.cpu.A = 0x1A
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x10
        self.ram[0x0011] = 0x20
        self.ram[0x2011] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_indirect_Y_A_bigger_than_M_boundary_cross(self):
        self.cpu.A = 0x1A
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xFF
        self.ram[0x0011] = 0x20
        self.ram[0x2100] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_A(0x1A).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_indirect_Y_A_equals_M(self):
        self.cpu.A = 0x1A
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x10
        self.ram[0x0011] = 0x20
        self.ram[0x2011] = 0x1A

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_A(0x1A).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_indirect_Y_A_less_than_M(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x10
        self.ram[0x0011] = 0x20
        self.ram[0x2011] = 0xFF

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_A(0x01).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_indirect_Y_A_less_than_M_with_negative_flag(self):
        self.cpu.A = 0x7F
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x10
        self.ram[0x0011] = 0x20
        self.ram[0x2011] = 0x80

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_A(0x7F).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_CMP_indirect_Y_A_greater_than_M_with_negative_flag(self):
        self.cpu.A = 0x8F
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CMP.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x10
        self.ram[0x0011] = 0x20
        self.ram[0x2011] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_A(0x8F).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))


class TestCpuCPXInstructions(CpuTestUtils):
    def test_CPX_immediate_X_bigger_than_M(self):
        self.cpu.X = 0x1A
        self.ram[0x1000] = instructions.CPX.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_X(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_immediate_X_equals_M(self):
        self.cpu.X = 0x1A
        self.ram[0x1000] = instructions.CPX.IMMEDIATE
        self.ram[0x1001] = 0x1A

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_X(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_immediate_X_less_than_M(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CPX.IMMEDIATE
        self.ram[0x1001] = 0xFF

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_immediate_X_less_than_M_with_negative_flag(self):
        self.cpu.X = 0x7F
        self.ram[0x1000] = instructions.CPX.IMMEDIATE
        self.ram[0x1001] = 0x80

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_X(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_immediate_X_greater_than_M_with_negative_flag(self):
        self.cpu.X = 0x8F
        self.ram[0x1000] = instructions.CPX.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_X(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_zero_page_X_bigger_than_M(self):
        self.cpu.X = 0x1A
        self.ram[0x1000] = instructions.CPX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_X(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_zero_page_X_equals_M(self):
        self.cpu.X = 0x1A
        self.ram[0x1000] = instructions.CPX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x1A

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_X(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_zero_page_X_less_than_M(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CPX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xFF

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_zero_page_X_less_than_M_with_negative_flag(self):
        self.cpu.X = 0x7F
        self.ram[0x1000] = instructions.CPX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x80

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_X(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_zero_page_X_greater_than_M_with_negative_flag(self):
        self.cpu.X = 0x8F
        self.ram[0x1000] = instructions.CPX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_X(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_absolute_X_bigger_than_M(self):
        self.cpu.X = 0x1A
        self.ram[0x1000] = instructions.CPX.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(True).has_X(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_absolute_X_equals_M(self):
        self.cpu.X = 0x1A
        self.ram[0x1000] = instructions.CPX.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(True).has_C(True).has_X(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_absolute_X_less_than_M(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.CPX.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(False).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_absolute_X_less_than_M_with_negative_flag(self):
        self.cpu.X = 0x7F
        self.ram[0x1000] = instructions.CPX.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(False).has_X(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_CPX_absolute_X_greater_than_M_with_negative_flag(self):
        self.cpu.X = 0x8F
        self.ram[0x1000] = instructions.CPX.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(True).has_X(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "X"))


class TestCpuCPYInstructions(CpuTestUtils):
    def test_CPY_immediate_Y_bigger_than_M(self):
        self.cpu.Y = 0x1A
        self.ram[0x1000] = instructions.CPY.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_Y(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_immediate_Y_equals_M(self):
        self.cpu.Y = 0x1A
        self.ram[0x1000] = instructions.CPY.IMMEDIATE
        self.ram[0x1001] = 0x1A

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_Y(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_immediate_Y_less_than_M(self):
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CPY.IMMEDIATE
        self.ram[0x1001] = 0xFF

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_immediate_Y_less_than_M_with_negative_flag(self):
        self.cpu.Y = 0x7F
        self.ram[0x1000] = instructions.CPY.IMMEDIATE
        self.ram[0x1001] = 0x80

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_Y(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_immediate_Y_greater_than_M_with_negative_flag(self):
        self.cpu.Y = 0x8F
        self.ram[0x1000] = instructions.CPY.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_Y(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_zero_page_Y_bigger_than_M(self):
        self.cpu.Y = 0x1A
        self.ram[0x1000] = instructions.CPY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(True).has_Y(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_zero_page_Y_equals_M(self):
        self.cpu.Y = 0x1A
        self.ram[0x1000] = instructions.CPY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x1A

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(True).has_C(True).has_Y(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_zero_page_Y_less_than_M(self):
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CPY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xFF

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(False).has_Z(False).has_C(False).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_zero_page_Y_less_than_M_with_negative_flag(self):
        self.cpu.Y = 0x7F
        self.ram[0x1000] = instructions.CPY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x80

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(False).has_Y(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_zero_page_Y_greater_than_M_with_negative_flag(self):
        self.cpu.Y = 0x8F
        self.ram[0x1000] = instructions.CPY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002)\
            .has_N(True).has_Z(False).has_C(True).has_Y(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_absolute_Y_bigger_than_M(self):
        self.cpu.Y = 0x1A
        self.ram[0x1000] = instructions.CPY.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(True).has_Y(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_absolute_Y_equals_M(self):
        self.cpu.Y = 0x1A
        self.ram[0x1000] = instructions.CPY.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(True).has_C(True).has_Y(0x1A)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_absolute_Y_less_than_M(self):
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.CPY.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(False).has_Z(False).has_C(False).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_absolute_Y_less_than_M_with_negative_flag(self):
        self.cpu.Y = 0x7F
        self.ram[0x1000] = instructions.CPY.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(False).has_Y(0x7F)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_CPY_absolute_Y_greater_than_M_with_negative_flag(self):
        self.cpu.Y = 0x8F
        self.ram[0x1000] = instructions.CPY.ABSOLUTE
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003)\
            .has_N(True).has_Z(False).has_C(True).has_Y(0x8F)\
            .has_no_changes(everything_except=("PC", "P", "Y"))
