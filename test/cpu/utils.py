import unittest
from typing import Iterable, Optional

from assertpy import assert_that, add_extension, remove_extension

from src.bus import Bus
from src.cpu import CPU
from src.device import RAM


def format_bin(num: int) -> str:
    return format(num, '#08b')


def format_hex_byte(num: int) -> str:
    return format(num, '#04X')


def format_hex_word(num: int) -> str:
    return format(num, '#06X')


def _flag_message(flag, message):
    return '{} flag is not {}'.format(flag, message)


def has_no_changes(self, values: Optional[Iterable] = None, everything_except: Optional[Iterable] = None):
    """ Possible values to check: A, SP, FC, P, X and Y """

    initial_values = {"A": 0, "SP": 0xFD, "PC": 0x1000, "P": 0x34, "X": 0, "Y": 0}

    if not isinstance(self.val, CPU):
        raise TypeError("This only tests a CPU")

    cpu = self.val

    if values and everything_except:
        raise ValueError("Either 'values' or 'everything_except' should be used. Not both.")

    if not values and not everything_except:
        values = initial_values.keys()

    things = values if values else set(initial_values.keys()) - set(everything_except)

    for t in things:
        if t not in initial_values:
            raise ValueError("{} is not a CPU attribute.".format(t))

        correct_value = initial_values[t]
        current_value = getattr(cpu, t)
        if current_value != correct_value:
            self.error("Expected {} to be <0x{:X}>, but it's <0x{:X}> instead.".format(t, correct_value, current_value))

    return self


class CpuTestUtils:
    def setup_method(self):
        self.create_resources()
        add_extension(has_no_changes)

    @staticmethod
    def teardown_method():
        remove_extension(has_no_changes)

    def create_resources(self):
        self.ram = RAM()
        self.ram[0xFFFC] = 0x00
        self.ram[0xFFFD] = 0x10

        bus = Bus()
        bus.add_device(self.ram, (0, 0xFFFF))

        self.cpu = CPU(bus)

    def run_clocks(self, clock_amount: int):
        for _ in range(clock_amount):
            self.cpu.clock()
