from typing import Dict, Tuple
from src.device import Device


class Bus(object):
    NoDevice = Device()

    def __init__(self):
        self.devices: Dict[Tuple[int, int], Device] = {}

    def add_device(self, device: Device, address_range: Tuple[int, int]):
        self.devices[address_range] = device

    def _get_device(self, address: int) -> Device:
        for addr_range, device in self.devices.items():
            addr_start, addr_end = addr_range
            if addr_start <= address <= addr_end:
                return device
        return Bus.NoDevice

    def read(self, address: int) -> int:
        return self._get_device(address).get(address)

    def write(self, address: int, data: int):
        self._get_device(address).set(address, data)
