from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuLDAInstructions(CpuTestUtils):
    def test_LDA_immediate(self):
        self.ram[0x1000] = instructions.LDA.IMMEDIATE
        self.ram[0x1001] = 0x10

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x10)\
            .has_no_changes(everything_except=("PC", "A"))

    def test_LDA_immediate_zero_flag(self):
        self.ram[0x1000] = instructions.LDA.IMMEDIATE
        self.ram[0x1001] = 0x0

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_LDA_immediate_negative_flag(self):
        self.ram[0x1000] = instructions.LDA.IMMEDIATE
        self.ram[0x1001] = 0x80

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_LDA_zero_page(self):
        self.ram[0x1000] = instructions.LDA.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x7B

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x7B)\
            .has_no_changes(everything_except=("PC", "A"))

    def test_LDA_zero_page_zero_flag(self):
        self.ram[0x1000] = instructions.LDA.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x00

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_LDA_zero_page_negative_flag(self):
        self.ram[0x1000] = instructions.LDA.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x80

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_LDA_zero_page_X(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0015] = 0x3A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x3A).has_X(0x05)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_LDA_zero_page_X_zero_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0015] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_X(0x05).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_LDA_zero_page_X_negative_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.ZERO_PAGE_X
        self.ram[0x1001] = 0x10
        self.ram[0x0015] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_X(0x05).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_LDA_absolute(self):
        self.ram[0x1000] = instructions.LDA.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x12

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x12)\
            .has_no_changes(everything_except=("PC", "A"))

    def test_LDA_absolute_zero_flag(self):
        self.ram[0x1000] = instructions.LDA.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_LDA_absolute_negative_flag(self):
        self.ram[0x1000] = instructions.LDA.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P"))

    def test_LDA_absolute_X(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_X(0x05)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_LDA_absolute_X_zero_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x00).has_X(0x05).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_LDA_absolute_X_negative_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_X
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x80).has_X(0x05).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_LDA_absolute_X_passing_boundaries(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2104] = 0x1A

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_X(0x05)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_LDA_absolute_Y(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_Y(0x05)\
            .has_no_changes(everything_except=("PC", "A", "Y"))

    def test_LDA_absolute_Y_zero_flag(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x00).has_Y(0x05).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_LDA_absolute_Y_negative_flag(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_Y
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x80).has_Y(0x05).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_LDA_absolute_Y_passing_boundaries(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDA.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2104] = 0x1A

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_A(0x1A).has_Y(0x05)\
            .has_no_changes(everything_except=("PC", "A", "Y"))

    def test_LDA_indirect_X(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0015] = 0x4A
        self.ram[0x0016] = 0x10
        self.ram[0x104A] = 0x20

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x20).has_X(0x05)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_LDA_indirect_X_zero_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0015] = 0x4A
        self.ram[0x0016] = 0x10
        self.ram[0x104A] = 0x00

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_X(0x05).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_LDA_indirect_X_negative_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDA.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0015] = 0x4A
        self.ram[0x0016] = 0x10
        self.ram[0x104A] = 0x80

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_X(0x05).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "X"))

    def test_LDA_indirect_Y(self):
        self.cpu.Y = 0x04
        self.ram[0x1000] = instructions.LDA.INDIRECT_Y
        self.ram[0x1001] = 0x02
        self.ram[0x0002] = 0x20
        self.ram[0x0003] = 0x10
        self.ram[0x1024] = 0x1B

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1B).has_Y(0x04)\
            .has_no_changes(everything_except=("PC", "A", "Y"))

    def test_LDA_indirect_Y_zero_flag(self):
        self.cpu.Y = 0x04
        self.ram[0x1000] = instructions.LDA.INDIRECT_Y
        self.ram[0x1001] = 0x02
        self.ram[0x0002] = 0x20
        self.ram[0x0003] = 0x10
        self.ram[0x1024] = 0x00

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x00).has_Y(0x04).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_LDA_indirect_Y_negative_flag(self):
        self.cpu.Y = 0x04
        self.ram[0x1000] = instructions.LDA.INDIRECT_Y
        self.ram[0x1001] = 0x02
        self.ram[0x0002] = 0x20
        self.ram[0x0003] = 0x10
        self.ram[0x1024] = 0x80

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x80).has_Y(0x04).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "A", "P", "Y"))

    def test_LDA_indirect_Y_passing_boundaries(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDA.INDIRECT_Y
        self.ram[0x1001] = 0x02
        self.ram[0x0002] = 0xFF
        self.ram[0x0003] = 0x10
        self.ram[0x1104] = 0x1B

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x1B).has_Y(0x05)\
            .has_no_changes(everything_except=("PC", "A", "Y"))


class TestCpuLDXInstructions(CpuTestUtils):
    def test_LDX_immediate(self):
        self.ram[0x1000] = instructions.LDX.IMMEDIATE
        self.ram[0x1001] = 0x10

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x10).has_no_changes(everything_except=("PC", "X"))

    def test_LDX_immediate_zero_flag(self):
        self.ram[0x1000] = instructions.LDX.IMMEDIATE
        self.ram[0x1001] = 0x00

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "X", "P"))

    def test_LDX_immediate_negative_flag(self):
        self.ram[0x1000] = instructions.LDX.IMMEDIATE
        self.ram[0x1001] = 0x80

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "X", "P"))

    def test_LDX_zero_page(self):
        self.ram[0x1000] = instructions.LDX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x0A

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x0A)\
            .has_no_changes(everything_except=("PC", "X"))

    def test_LDX_zero_page_zero_flag(self):
        self.ram[0x1000] = instructions.LDX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x00

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "X", "P"))

    def test_LDX_zero_page_negative_flag(self):
        self.ram[0x1000] = instructions.LDX.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x80

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "X", "P"))

    def test_LDX_zero_page_Y(self):
        self.cpu.Y = 0x02
        self.ram[0x1000] = instructions.LDX.ZERO_PAGE_Y
        self.ram[0x1001] = 0x12
        self.ram[0x0014] = 0x1B

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x1B).has_Y(0x02)

    def test_LDX_zero_page_Y_zero_flag(self):
        self.cpu.Y = 0x02
        self.ram[0x1000] = instructions.LDX.ZERO_PAGE_Y
        self.ram[0x1001] = 0x12
        self.ram[0x0014] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x00).has_Y(0x02).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "X", "Y", "P"))

    def test_LDX_zero_page_Y_negative_flag(self):
        self.cpu.Y = 0x02
        self.ram[0x1000] = instructions.LDX.ZERO_PAGE_Y
        self.ram[0x1001] = 0x12
        self.ram[0x0014] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_X(0x80).has_Y(0x02).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "X", "Y", "P"))

    def test_LDX_absolute(self):
        self.ram[0x1000] = instructions.LDX.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x2C

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x2C)\
            .has_no_changes(everything_except=("PC", "X"))

    def test_LDX_absolute_zero_flag(self):
        self.ram[0x1000] = instructions.LDX.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "X", "P"))

    def test_LDX_absolute_negative_flag(self):
        self.ram[0x1000] = instructions.LDX.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "X", "P"))

    def test_LDX_absolute_Y(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDX.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1025] = 0x4D

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x4D).has_Y(0x05)\
            .has_no_changes(everything_except=("PC", "X", "Y"))

    def test_LDX_absolute_Y_zero_flag(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDX.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1025] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x00).has_Y(0x05).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "X", "Y", "P"))

    def test_LDX_absolute_Y_negative_flag(self):
        self.cpu.Y = 0x05
        self.ram[0x1000] = instructions.LDX.ABSOLUTE_Y
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1025] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x80).has_Y(0x05).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "X", "Y", "P"))

    def test_LDX_absolute_Y_passing_boundaries(self):
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.LDX.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x4D

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x4D).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "X", "Y"))


class TestCpuLDYInstructions(CpuTestUtils):
    def test_LDY_immediate(self):
        self.ram[0x1000] = instructions.LDY.IMMEDIATE
        self.ram[0x1001] = 0x10

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x10)\
            .has_no_changes(everything_except=("PC", "Y"))

    def test_LDY_immediate_zero_flag(self):
        self.ram[0x1000] = instructions.LDY.IMMEDIATE
        self.ram[0x1001] = 0x00

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "Y", "P"))

    def test_LDY_immediate_negative_flag(self):
        self.ram[0x1000] = instructions.LDY.IMMEDIATE
        self.ram[0x1001] = 0x80

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "Y", "P"))

    def test_LDY_zero_page(self):
        self.ram[0x1000] = instructions.LDY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x0A

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x0A)\
            .has_no_changes(everything_except=("PC", "Y"))

    def test_LDY_zero_page_zero_flag(self):
        self.ram[0x1000] = instructions.LDY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x00

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "Y", "P"))

    def test_LDY_zero_page_negative_flag(self):
        self.ram[0x1000] = instructions.LDY.ZERO_PAGE
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x80

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "Y", "P"))

    def test_LDY_zero_page_X(self):
        self.cpu.X = 0x02
        self.ram[0x1000] = instructions.LDY.ZERO_PAGE_X
        self.ram[0x1001] = 0x12
        self.ram[0x0014] = 0x1B

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x1B).has_X(0x02)\
            .has_no_changes(everything_except=("PC", "Y", "X"))

    def test_LDY_zero_page_X_zero_flag(self):
        self.cpu.X = 0x02
        self.ram[0x1000] = instructions.LDY.ZERO_PAGE_X
        self.ram[0x1001] = 0x12
        self.ram[0x0014] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x00).has_X(0x02).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "Y", "X", "P"))

    def test_LDY_zero_page_X_negative_flag(self):
        self.cpu.X = 0x02
        self.ram[0x1000] = instructions.LDY.ZERO_PAGE_X
        self.ram[0x1001] = 0x12
        self.ram[0x0014] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_Y(0x80).has_X(0x02).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "Y", "X", "P"))

    def test_LDY_absolute(self):
        self.ram[0x1000] = instructions.LDY.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x2C

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_Y(0x2C)\
            .has_no_changes(everything_except=("PC", "Y"))

    def test_LDY_absolute_zero_flag(self):
        self.ram[0x1000] = instructions.LDY.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_Y(0x00).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "Y", "P"))

    def test_LDY_absolute_negative_flag(self):
        self.ram[0x1000] = instructions.LDY.ABSOLUTE
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1020] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_Y(0x80).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "Y", "P"))

    def test_LDY_absolute_X(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDY.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1025] = 0x4D

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_Y(0x4D).has_X(0x05)\
            .has_no_changes(everything_except=("PC", "Y", "X"))

    def test_LDY_absolute_X_zero_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDY.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1025] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_Y(0x00).has_X(0x05).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "Y", "X", "P"))

    def test_LDY_absolute_X_negative_flag(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.LDY.ABSOLUTE_X
        self.ram[0x1001] = 0x20
        self.ram[0x1002] = 0x10
        self.ram[0x1025] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_Y(0x80).has_X(0x05).has_Z(False).has_N(True)\
            .has_no_changes(everything_except=("PC", "Y", "X", "P"))

    def test_LDY_absolute_X_page_crossed(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.LDY.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x10
        self.ram[0x1100] = 0x4D

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_Y(0x4D).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "Y", "X"))


class TestCpuSTAInstructions(CpuTestUtils):
    def setup_method(self):
        super(TestCpuSTAInstructions, self).setup_method()
        self.new_A = 0xAB
        self.cpu.A = self.new_A

    def test_STA_zero_page(self):
        self.ram[0x1000] = instructions.STA.ZERO_PAGE
        self.ram[0x1001] = 0x10

        self.run_clocks(3)
        assert_that(self.ram[0x0010]).is_equal_to(self.new_A)
        assert_that(self.cpu).has_PC(0x1002).has_A(self.new_A)\
            .has_no_changes(everything_except=("PC", "A"))

    def test_STA_zero_page_X(self):
        self.cpu.X = 0x05
        self.ram[0x1000] = instructions.STA.ZERO_PAGE_X
        self.ram[0x1001] = 0x10

        self.run_clocks(4)
        assert_that(self.ram[0x0015]).is_equal_to(self.new_A)
        assert_that(self.cpu).has_PC(0x1002).has_A(self.new_A).has_X(0x05)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_STA_absolute(self):
        self.ram[0x1000] = instructions.STA.ABSOLUTE
        self.ram[0x1001] = 0x34
        self.ram[0x1002] = 0x12

        self.run_clocks(4)
        assert_that(self.ram[0x1234]).is_equal_to(self.new_A)
        assert_that(self.cpu).has_PC(0x1003).has_A(self.new_A)\
            .has_no_changes(everything_except=("PC", "A"))

    def test_STA_absolute_X(self):
        self.cpu.X = 0x02
        self.ram[0x1000] = instructions.STA.ABSOLUTE_X
        self.ram[0x1001] = 0x34
        self.ram[0x1002] = 0x12

        self.run_clocks(5)
        assert_that(self.ram[0x1236]).is_equal_to(self.new_A)
        assert_that(self.cpu).has_PC(0x1003).has_A(self.new_A).has_X(0x02)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_STA_absolute_Y(self):
        self.cpu.Y = 0x03
        self.ram[0x1000] = instructions.STA.ABSOLUTE_Y
        self.ram[0x1001] = 0x34
        self.ram[0x1002] = 0x12

        self.run_clocks(5)
        assert_that(self.ram[0x1237]).is_equal_to(self.new_A)
        assert_that(self.cpu).has_PC(0x1003).has_A(self.new_A).has_Y(0x03)\
            .has_no_changes(everything_except=("PC", "A", "Y"))

    def test_STA_indirect_X(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.STA.INDIRECT_X
        self.ram[0x1001] = 0x12
        self.ram[0x0013] = 0xFF
        self.ram[0x0014] = 0x00

        self.run_clocks(6)
        assert_that(self.ram[0x00FF]).is_equal_to(self.new_A)
        assert_that(self.cpu).has_PC(0x1002).has_A(self.new_A).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_STA_indirect_Y(self):
        self.cpu.Y = 0x02
        self.ram[0x1000] = instructions.STA.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xCD
        self.ram[0x0011] = 0xAB

        self.run_clocks(6)
        assert_that(self.ram[0xABCF]).is_equal_to(self.new_A)
        assert_that(self.cpu).has_PC(0x1002).has_A(self.new_A).has_Y(0x02)\
            .has_no_changes(everything_except=("PC", "A", "Y"))


class TestCpuSTXInstructions(CpuTestUtils):
    def setup_method(self) -> None:
        super(TestCpuSTXInstructions, self).setup_method()
        self.new_X = 0xAB
        self.cpu.X = self.new_X

    def test_STX_zero_page(self):
        self.ram[0x1000] = instructions.STX.ZERO_PAGE
        self.ram[0x1001] = 0x10

        self.run_clocks(3)
        assert_that(self.ram[0x0010]).is_equal_to(self.new_X)
        assert_that(self.cpu).has_PC(0x1002).has_X(self.new_X)\
            .has_no_changes(everything_except=("PC", "X"))

    def test_STX_zero_page_Y(self):
        self.cpu.Y = 0x10
        self.ram[0x1000] = instructions.STX.ZERO_PAGE_Y
        self.ram[0x1001] = 0x10

        self.run_clocks(4)
        assert_that(self.ram[0x0020]).is_equal_to(self.new_X)
        assert_that(self.cpu).has_PC(0x1002).has_X(self.new_X).has_Y(0x10)\
            .has_no_changes(everything_except=("PC", "X", "Y"))

    def test_STX_absolute(self):
        self.ram[0x1000] = instructions.STX.ABSOLUTE
        self.ram[0x1001] = 0xDE
        self.ram[0x1002] = 0xBC

        self.run_clocks(4)
        assert_that(self.ram[0xBCDE]).is_equal_to(self.new_X)
        assert_that(self.cpu).has_PC(0x1003).has_X(self.new_X)\
            .has_no_changes(everything_except=("PC", "X"))


class TestCpuSTYInstructions(CpuTestUtils):
    def setup_method(self) -> None:
        super(TestCpuSTYInstructions, self).setup_method()
        self.new_Y = 0xAB
        self.cpu.Y = self.new_Y

    def test_STY_zero_page(self):
        self.ram[0x1000] = instructions.STY.ZERO_PAGE
        self.ram[0x1001] = 0x10

        self.run_clocks(3)
        assert_that(self.ram[0x0010]).is_equal_to(self.new_Y)
        assert_that(self.cpu).has_PC(0x1002).has_Y(self.new_Y)\
            .has_no_changes(everything_except=("PC", "Y"))

    def test_STY_zero_page_X(self):
        self.cpu.X = 0x10
        self.ram[0x1000] = instructions.STY.ZERO_PAGE_X
        self.ram[0x1001] = 0x10

        self.run_clocks(4)
        assert_that(self.ram[0x0020]).is_equal_to(self.new_Y)
        assert_that(self.cpu).has_PC(0x1002).has_Y(self.new_Y).has_X(0x10)\
            .has_no_changes(everything_except=("PC", "Y", "X"))

    def test_STY_absolute(self):
        self.ram[0x1000] = instructions.STY.ABSOLUTE
        self.ram[0x1001] = 0xDE
        self.ram[0x1002] = 0xBC

        self.run_clocks(4)
        assert_that(self.ram[0xBCDE]).is_equal_to(self.new_Y)
        assert_that(self.cpu).has_PC(0x1003).has_Y(self.new_Y)\
            .has_no_changes(everything_except=("PC", "Y"))
