from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuTAXInstructions(CpuTestUtils):
    def test_TAX_implied(self):
        self.cpu.A = 0x12
        self.ram[0x1000] = instructions.TAX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_A(0x12).has_X(0x12)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_TAX_implied_zero_flag(self):
        self.cpu.A = 0x00
        self.ram[0x1000] = instructions.TAX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "P"))

    def test_TAX_implied_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.TAX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_N(True).has_Z(False).has_A(0x80).has_X(0x80)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))


class TestCpuTXAInstructions(CpuTestUtils):
    def test_TXA_implied(self):
        self.cpu.X = 0x23
        self.ram[0x1000] = instructions.TXA.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(0x23).has_A(0x23)\
            .has_no_changes(everything_except=("PC", "A", "X"))

    def test_TXA_implied_zero_flag(self):
        self.cpu.X = 0x00
        self.ram[0x1000] = instructions.TXA.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "P"))

    def test_TXA_implied_negative_flag(self):
        self.cpu.X = 0x80
        self.ram[0x1000] = instructions.TXA.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_N(True).has_Z(False).has_A(0x80).has_X(0x80)\
            .has_no_changes(everything_except=("PC", "A", "X", "P"))


class TestCpuTAYInstructions(CpuTestUtils):
    def test_TAY_implied(self):
        self.cpu.A = 0x14
        self.ram[0x1000] = instructions.TAY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_A(0x14).has_Y(0x14)\
            .has_no_changes(everything_except=("PC", "A", "Y"))

    def test_TAY_implied_zero_flag(self):
        self.cpu.A = 0x00
        self.ram[0x1000] = instructions.TAY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "P"))

    def test_TAY_implied_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.TAY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_N(True).has_Z(False).has_A(0x80).has_Y(0x80)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))


class TestCpuTYAInstructions(CpuTestUtils):
    def test_TYA_implied(self):
        self.cpu.Y = 0x1B
        self.ram[0x1000] = instructions.TYA.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Y(0x1B).has_A(0x1B)\
            .has_no_changes(everything_except=("PC", "A", "Y"))

    def test_TYA_implied_zero_flag(self):
        self.cpu.Y = 0x00
        self.ram[0x1000] = instructions.TYA.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "P"))

    def test_TYA_implied_negative_flag(self):
        self.cpu.Y = 0x80
        self.ram[0x1000] = instructions.TYA.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_N(True).has_Z(False).has_Y(0x80).has_A(0x80)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))


class TestCpuTXSInstructions(CpuTestUtils):
    def test_TXS_implied(self):
        self.cpu.X = 0xAD
        self.ram[0x1000] = instructions.TXS.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_SP(0xAD).has_X(0xAD)\
            .has_no_changes(everything_except=("PC", "SP", "X"))

    def test_TXS_not_affect_flags(self):
        self.cpu.X = 0x00
        self.ram[0x1000] = instructions.TXS.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_SP(0x00).has_X(0x00)\
            .has_no_changes(everything_except=("PC", "SP", "X"))

        self.cpu.X = 0x80
        self.ram[0x1001] = instructions.TXS.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_SP(0x80).has_X(0x80)\
            .has_no_changes(everything_except=("PC", "SP", "X"))


class TestCpuTSXInstructions(CpuTestUtils):
    def test_TSX_implied(self):
        # starting stack has bit 7 set
        new_p = self.cpu.P | (1 << 7)
        self.ram[0x1000] = instructions.TSX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(self.cpu.SP).has_P(new_p)\
            .has_no_changes(everything_except=("PC", "X", "P"))

    def test_TSX_implied_zero_flag(self):
        # starting stack has bit 7 set
        self.cpu.SP = 0x00
        self.ram[0x1000] = instructions.TSX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Z(True).has_N(False).has_SP(0x00)\
            .has_no_changes(everything_except=("PC", "P", "SP"))

    def test_TSX_implied_negative_flag(self):
        # starting stack has bit 7 set
        self.cpu.SP = 0x80
        self.ram[0x1000] = instructions.TSX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_N(True).has_Z(False).has_X(0x80).has_SP(0x80)\
            .has_no_changes(everything_except=("PC", "SP", "P", "X"))
