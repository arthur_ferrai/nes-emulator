from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuANDInstructions(CpuTestUtils):
    def test_AND_immediate_zero_flag(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.AND.IMMEDIATE
        self.ram[0x1001] = 0x10

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_AND_immediate_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.AND.IMMEDIATE
        self.ram[0x1001] = 0xFF

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x80)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_AND_zero_page_zero_flag(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.AND.ZERO_PAGE
        self.ram[0x1001] = 0x15
        self.ram[0x0015] = 0x10

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_AND_zero_page_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.AND.ZERO_PAGE
        self.ram[0x1001] = 0x15
        self.ram[0x0015] = 0xFF

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x80)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_AND_zero_page_X_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_zero_page_X_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.ZERO_PAGE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_zero_page_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x80).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_absolute_zero_flag(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.AND.ABSOLUTE
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_AND_absolute_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.AND.ABSOLUTE
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x80)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_AND_absolute_X_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.ABSOLUTE_X
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_absolute_X_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x10

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_absolute_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.ABSOLUTE_X
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x80).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_absolute_Y_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.AND.ABSOLUTE_Y
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x10

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_AND_absolute_Y_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.AND.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x10

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_AND_absolute_Y_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.AND.ABSOLUTE_Y
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0xFF

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x80).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_AND_indirect_X_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x15
        self.ram[0x0015] = 0x10

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_indirect_X_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.INDIRECT_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x15
        self.ram[0x0015] = 0x10

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_indirect_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.AND.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x15
        self.ram[0x0015] = 0xFF

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x80).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_AND_indirect_Y_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.AND.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x15
        self.ram[0x0011] = 0x20
        self.ram[0x2016] = 0x10

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_AND_indirect_Y_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.AND.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xFF
        self.ram[0x0011] = 0x20
        self.ram[0x2100] = 0x10

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_AND_indirect_Y_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.AND.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x15
        self.ram[0x0011] = 0x20
        self.ram[0x2016] = 0xFF

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x80).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))


class TestCpuEORInstructions(CpuTestUtils):
    def test_EOR_immediate_zero_flag(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.EOR.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_EOR_immediate_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.EOR.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_EOR_zero_page_zero_flag(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.EOR.ZERO_PAGE
        self.ram[0x1001] = 0x15
        self.ram[0x0015] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_EOR_zero_page_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.EOR.ZERO_PAGE
        self.ram[0x1001] = 0x15
        self.ram[0x0015] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_EOR_zero_page_X_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_zero_page_X_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.ZERO_PAGE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_zero_page_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_absolute_zero_flag(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.EOR.ABSOLUTE
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_EOR_absolute_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.EOR.ABSOLUTE
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x81)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_EOR_absolute_X_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.ABSOLUTE_X
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_absolute_X_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_absolute_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.ABSOLUTE_X
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x81).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_absolute_Y_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.EOR.ABSOLUTE_Y
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_EOR_absolute_Y_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.EOR.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_EOR_absolute_Y_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.EOR.ABSOLUTE_Y
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x81).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_EOR_indirect_X_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x15
        self.ram[0x0015] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_indirect_X_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.INDIRECT_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x15
        self.ram[0x0015] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_indirect_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.EOR.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x15
        self.ram[0x0015] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_EOR_indirect_Y_zero_flag(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.EOR.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x15
        self.ram[0x0011] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_EOR_indirect_Y_boundary_cross(self):
        self.cpu.A = 0x01
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.EOR.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xFF
        self.ram[0x0011] = 0x20
        self.ram[0x2100] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_EOR_indirect_Y_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.EOR.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x15
        self.ram[0x0011] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))


class TestCpuORAInstructions(CpuTestUtils):
    def test_ORA_immediate_zero_flag(self):
        self.cpu.A = 0x00
        self.ram[0x1000] = instructions.ORA.IMMEDIATE
        self.ram[0x1001] = 0x00

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_ORA_immediate_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.ORA.IMMEDIATE
        self.ram[0x1001] = 0x01

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_ORA_zero_page_zero_flag(self):
        self.cpu.A = 0x00
        self.ram[0x1000] = instructions.ORA.ZERO_PAGE
        self.ram[0x1001] = 0x15
        self.ram[0x0015] = 0x00

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_ORA_zero_page_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.ORA.ZERO_PAGE
        self.ram[0x1001] = 0x15
        self.ram[0x0015] = 0x01

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_ORA_zero_page_X_zero_flag(self):
        self.cpu.A = 0x00
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_zero_page_X_boundary_cross(self):
        self.cpu.A = 0x00
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.ZERO_PAGE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_zero_page_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.ZERO_PAGE_X
        self.ram[0x1001] = 0x15
        self.ram[0x0016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_absolute_zero_flag(self):
        self.cpu.A = 0x00
        self.ram[0x1000] = instructions.ORA.ABSOLUTE
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_ORA_absolute_negative_flag(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.ORA.ABSOLUTE
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2015] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x81)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_ORA_absolute_X_zero_flag(self):
        self.cpu.A = 0x00
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.ABSOLUTE_X
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_absolute_X_boundary_cross(self):
        self.cpu.A = 0x00
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.ABSOLUTE_X
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x00

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_absolute_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.ABSOLUTE_X
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x81).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_absolute_Y_zero_flag(self):
        self.cpu.A = 0x00
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ORA.ABSOLUTE_Y
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_ORA_absolute_Y_boundary_cross(self):
        self.cpu.A = 0x00
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ORA.ABSOLUTE_Y
        self.ram[0x1001] = 0xFF
        self.ram[0x1002] = 0x20
        self.ram[0x2100] = 0x00

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_ORA_absolute_Y_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ORA.ABSOLUTE_Y
        self.ram[0x1001] = 0x15
        self.ram[0x1002] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_A(0x81).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_ORA_indirect_X_zero_flag(self):
        self.cpu.A = 0x00
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x15
        self.ram[0x0015] = 0x00

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_indirect_X_boundary_cross(self):
        self.cpu.A = 0x00
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.INDIRECT_X
        self.ram[0x1001] = 0xFF
        self.ram[0x0000] = 0x15
        self.ram[0x0015] = 0x00

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_indirect_X_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.ORA.INDIRECT_X
        self.ram[0x1001] = 0x10
        self.ram[0x0011] = 0x15
        self.ram[0x0015] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "X"))

    def test_ORA_indirect_Y_zero_flag(self):
        self.cpu.A = 0x00
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ORA.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x15
        self.ram[0x0011] = 0x20
        self.ram[0x2016] = 0x00

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_ORA_indirect_Y_boundary_cross(self):
        self.cpu.A = 0x00
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ORA.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0xFF
        self.ram[0x0011] = 0x20
        self.ram[0x2100] = 0x00

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_A(0x00).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))

    def test_ORA_indirect_Y_negative_flag(self):
        self.cpu.A = 0x80
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.ORA.INDIRECT_Y
        self.ram[0x1001] = 0x10
        self.ram[0x0010] = 0x15
        self.ram[0x0011] = 0x20
        self.ram[0x2016] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_A(0x81).has_Y(0x01)\
            .has_no_changes(everything_except=("PC", "P", "A", "Y"))


class TestCpuBITInstructions(CpuTestUtils):
    def test_BIT_zero_page_z_v_n_false(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x11

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_zero_page_z_v_false_n_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x81

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_zero_page_z_n_false_v_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x41

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_zero_page_v_n_false_z_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x00

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_zero_page_z_false_v_n_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0xC1

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_zero_page_v_false_z_n_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x80

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(True).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_zero_page_n_false_z_v_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x40

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_zero_page_n_z_v_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0xC0

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(True).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_z_v_n_false(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x11

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(False).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_z_v_false_n_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x81

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_z_n_false_v_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x41

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(False).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_v_n_false_z_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_z_false_v_n_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0xC1

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_v_false_z_n_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(True).has_V(False).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_n_false_z_v_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x40

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))

    def test_BIT_absolute_n_z_v_true(self):
        self.cpu.A = 0x0F
        self.ram[0x1000] = instructions.BIT.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0xC0

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(True).has_V(True).has_A(0x0F)\
            .has_no_changes(everything_except=("PC", "P", "A"))
