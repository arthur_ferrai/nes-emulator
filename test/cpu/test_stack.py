from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuPHAInstructions(CpuTestUtils):
    def test_PHA_implied(self):
        current_stack = self.cpu.SP
        self.cpu.A = 0x4C
        self.ram[0x1000] = instructions.PHA.IMPLIED

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1001).has_A(0x4C).has_SP(current_stack - 1)\
            .has_no_changes(everything_except=("PC", "A", "SP"))
        assert_that(self.ram[0x100 + current_stack]).is_equal_to(0x4C)


class TestCpuPHPInstructions(CpuTestUtils):
    def test_PHP_implied(self):
        current_stack = self.cpu.SP
        self.cpu.P = 0b10110100
        self.ram[0x1000] = instructions.PHP.IMPLIED

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0x1001).has_P(0b10110100).has_SP(current_stack - 1)\
            .has_no_changes(everything_except=("PC", "P", "SP"))
        assert_that(self.ram[0x100 + current_stack]).is_equal_to(0b10110100)


class TestCpuPLAInstructions(CpuTestUtils):
    def test_PLA_implied(self):
        self.ram[0x1000] = instructions.PLA.IMPLIED
        self.ram[0x01FE] = 0x1A

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1001).has_SP(0xFE).has_A(0x1A)\
            .has_no_changes(everything_except=("PC", "SP", "A"))

    def test_PLA_implied_zero_flag(self):
        self.ram[0x1000] = instructions.PLA.IMPLIED
        self.ram[0x01FE] = 0x00

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1001).has_SP(0xFE).has_Z(True).has_N(False)\
            .has_no_changes(everything_except=("PC", "SP", "P"))

    def test_PLA_implied_negative_flag(self):
        self.ram[0x1000] = instructions.PLA.IMPLIED
        self.ram[0x01FE] = 0x80

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1001).has_SP(0xFE).has_N(True).has_Z(False).has_A(0x80)\
            .has_no_changes(everything_except=("PC", "SP", "P", "A"))


class TestCpuPLPInstructions(CpuTestUtils):
    def test_PLP_implied(self):
        new_p = 0b10110100
        assert_that(self.cpu.P).is_not_equal_to(new_p)

        self.ram[0x1000] = instructions.PLP.IMPLIED
        self.ram[0x01FE] = new_p

        self.run_clocks(4)
        assert_that(self.cpu).has_PC(0x1001).has_SP(0xFE).has_P(new_p)\
            .has_no_changes(everything_except=("PC", "SP", "P"))
