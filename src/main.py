from bus import Bus
from cpu import CPU
from src.device.ram import RAM


def main():
    bus = Bus()
    ram = RAM()
    bus.add_device(ram, (0x0000, 0xFFFF))
    cpu = CPU(bus)


if __name__ == "__main__":
    main()
