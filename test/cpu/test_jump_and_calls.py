from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuJMPInstructions(CpuTestUtils):
    def test_JMP_absolute(self):
        self.ram[0x1000] = instructions.JMP.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB

        self.run_clocks(3)
        assert_that(self.cpu).has_PC(0xABCD)\
            .has_no_changes(everything_except=("PC",))

    def test_JMP_indirect(self):
        self.ram[0x1000] = instructions.JMP.INDIRECT
        self.ram[0x1001] = 0x10
        self.ram[0x1002] = 0x20
        self.ram[0x2010] = 0x12
        self.ram[0x2011] = 0xDE

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0xDE12)\
            .has_no_changes(everything_except=("PC",))


class TestCpuJSRInstructions(CpuTestUtils):
    def test_JSR_absolute(self):
        self.ram[0x1000] = instructions.JSR.ABSOLUTE
        self.ram[0x1001] = 0x34
        self.ram[0x1002] = 0x12

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1234).has_SP(0xFB)\
            .has_no_changes(everything_except=("PC", "SP"))
        assert_that(self.ram[0x01FC]).is_equal_to(0x02)
        assert_that(self.ram[0x01FD]).is_equal_to(0x10)


class TestCpuRTSInstructions(CpuTestUtils):
    def setup_method(self):
        super(TestCpuRTSInstructions, self).setup_method()
        self.ram[0x1000] = instructions.JSR.ABSOLUTE
        self.ram[0x1001] = 0x34
        self.ram[0x1002] = 0x12
        self.run_clocks(6)

    def test_RTS_implied(self):
        self.ram[0x1234] = instructions.RTS.IMPLIED

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_SP(0xFD)\
            .has_no_changes(everything_except=("PC", "SP"))
