import pytest
from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuASLInstructions(CpuTestUtils):
    ...


class TestCpuASLAccumulator(TestCpuASLInstructions):
    def _asset_accumulator_does_not_change_anything_else(self):
        assert_that(self.cpu).has_no_changes(everything_except=('PC', 'A', 'P', 'C', 'Z', 'N'))

    def test_ASL_accumulator_c_z_n_false(self):
        self.cpu.A = 0x01
        self.ram[0x1000] = instructions.ASL.ACCUMULATOR
        self.ram[0x1001] = instructions.ASL.ACCUMULATOR

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_A(0x02).has_Z(False).has_C(False).has_N(False)

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1002).has_A(0x04).has_Z(False).has_C(False).has_N(False)
        self._asset_accumulator_does_not_change_anything_else()

    def test_ASL_accumulator_c_true_z_n_false(self):
        self.cpu.A = 0x82
        self.ram[0x1000] = instructions.ASL.ACCUMULATOR

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_A(0x04).has_Z(False).has_C(True).has_N(False)
        self._asset_accumulator_does_not_change_anything_else()

    def test_ASL_accumulator_c_z_true_n_false(self):
        self.cpu.A = 0x80
        self.ram[0x1000] = instructions.ASL.ACCUMULATOR

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_A(0x00).has_Z(True).has_C(True).has_N(False)
        self._asset_accumulator_does_not_change_anything_else()

    def test_ASL_accumulator_c_n_true_z_false(self):
        self.cpu.A = 0xC0
        self.ram[0x1000] = instructions.ASL.ACCUMULATOR

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_A(0x80).has_Z(False).has_C(True).has_N(True)
        self._asset_accumulator_does_not_change_anything_else()


class TestCpuASLZeroPage(TestCpuASLInstructions):
    def _assert_zero_page_does_not_change_anything_else(self):
        assert_that(self.cpu).has_no_changes(everything_except=('PC', 'P', 'C', 'Z', 'N'))

    def test_ASL_zero_page_c_z_n_false(self):
        self.ram[0x0001] = 0x01

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE
        self.ram[0x1001] = 0x01

        self.ram[0x1002] = instructions.ASL.ZERO_PAGE
        self.ram[0x1003] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_Z(False).has_C(False).has_N(False)
        assert_that(self.ram[0x0001]).is_equal_to(0x02)

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1004).has_Z(False).has_C(False).has_N(False)
        assert_that(self.ram[0x0001]).is_equal_to(0x04)
        self._assert_zero_page_does_not_change_anything_else()

    def test_ASL_zero_page_c_true_z_n_false(self):
        self.ram[0x0001] = 0x82

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE
        self.ram[0x1001] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_Z(False).has_C(True).has_N(False)
        assert_that(self.ram[0x0001]).is_equal_to(0x04)
        self._assert_zero_page_does_not_change_anything_else()

    def test_ASL_zero_page_c_z_true_n_false(self):
        self.ram[0x0001] = 0x80

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE
        self.ram[0x1001] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_Z(True).has_C(True).has_N(False)
        assert_that(self.ram[0x0001]).is_equal_to(0x00)
        self._assert_zero_page_does_not_change_anything_else()

    def test_ASL_zero_page_c_n_true_z_false(self):
        self.ram[0x0001] = 0xC0

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE
        self.ram[0x1001] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_Z(False).has_C(True).has_N(True)
        assert_that(self.ram[0x0001]).is_equal_to(0x80)
        self._assert_zero_page_does_not_change_anything_else()


class TestCpuASLZeroPageX(TestCpuASLInstructions):
    def _assert_zero_page_X_does_not_change_anything_else(self):
        assert_that(self.cpu).has_no_changes(everything_except=('PC', 'P', 'C', 'Z', 'N', 'X'))

    def test_ASL_zero_page_X_c_z_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x0002] = 0x01

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE_X
        self.ram[0x1001] = 0x01

        self.ram[0x1002] = instructions.ASL.ZERO_PAGE_X
        self.ram[0x1003] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_Z(False).has_C(False).has_N(False)
        assert_that(self.ram[0x0002]).is_equal_to(0x02)

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1004).has_Z(False).has_C(False).has_N(False)
        assert_that(self.ram[0x0002]).is_equal_to(0x04)
        self._assert_zero_page_X_does_not_change_anything_else()

    def test_ASL_zero_page_X_boundary_check(self):
        self.cpu.X = 0x01
        self.ram[0x0000] = 0x01

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE_X
        self.ram[0x1001] = 0xFF

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_Z(False).has_C(False).has_N(False)
        assert_that(self.ram[0x0000]).is_equal_to(0x02)

        self._assert_zero_page_X_does_not_change_anything_else()

    def test_ASL_zero_page_X_c_true_z_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x0002] = 0x82

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE_X
        self.ram[0x1001] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_Z(False).has_C(True).has_N(False)
        assert_that(self.ram[0x0002]).is_equal_to(0x04)
        self._assert_zero_page_X_does_not_change_anything_else()

    def test_ASL_zero_page_X_c_z_true_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x0002] = 0x80

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE_X
        self.ram[0x1001] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_Z(True).has_C(True).has_N(False)
        assert_that(self.ram[0x0002]).is_equal_to(0x00)
        self._assert_zero_page_X_does_not_change_anything_else()

    def test_ASL_zero_page_X_c_n_true_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x0002] = 0xC0

        self.ram[0x1000] = instructions.ASL.ZERO_PAGE_X
        self.ram[0x1001] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_Z(False).has_C(True).has_N(True)
        assert_that(self.ram[0x0002]).is_equal_to(0x80)
        self._assert_zero_page_X_does_not_change_anything_else()


class TestCpuASLAbsolute(TestCpuASLInstructions):
    def _assert_absolute_does_not_change_anything_else(self):
        assert_that(self.cpu).has_no_changes(everything_except=('PC', 'P', 'C', 'Z', 'N'))

    def test_ASL_absolute_c_z_n_false(self):
        self.ram[0x0123] = 0x01

        self.ram[0x1000] = instructions.ASL.ABSOLUTE
        self.ram[0x1001] = 0x23
        self.ram[0x1002] = 0x01

        self.ram[0x1003] = instructions.ASL.ABSOLUTE
        self.ram[0x1004] = 0x23
        self.ram[0x1005] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_Z(False).has_C(False).has_N(False)
        assert_that(self.ram[0x0123]).is_equal_to(0x02)

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1006).has_Z(False).has_C(False).has_N(False)
        assert_that(self.ram[0x0123]).is_equal_to(0x04)
        self._assert_absolute_does_not_change_anything_else()

    def test_ASL_absolute_c_true_z_n_false(self):
        self.ram[0x0123] = 0x82

        self.ram[0x1000] = instructions.ASL.ABSOLUTE
        self.ram[0x1001] = 0x23
        self.ram[0x1002] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_Z(False).has_C(True).has_N(False)
        assert_that(self.ram[0x0123]).is_equal_to(0x04)
        self._assert_absolute_does_not_change_anything_else()

    def test_ASL_absolute_c_z_true_n_false(self):
        self.ram[0x0123] = 0x80

        self.ram[0x1000] = instructions.ASL.ABSOLUTE
        self.ram[0x1001] = 0x23
        self.ram[0x1002] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_Z(True).has_C(True).has_N(False)
        assert_that(self.ram[0x0123]).is_equal_to(0x00)
        self._assert_absolute_does_not_change_anything_else()

    def test_ASL_absolute_c_n_true_z_false(self):
        self.ram[0x0123] = 0xC0

        self.ram[0x1000] = instructions.ASL.ABSOLUTE
        self.ram[0x1001] = 0x23
        self.ram[0x1002] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_Z(False).has_C(True).has_N(True)
        assert_that(self.ram[0x0123]).is_equal_to(0x80)
        self._assert_absolute_does_not_change_anything_else()


class TestCpuASLAbsoluteX(TestCpuASLInstructions):
    def _assert_absolute_X_does_not_change_anything_else(self):
        assert_that(self.cpu).has_no_changes(everything_except=('PC', 'P', 'C', 'Z', 'N', 'X'))

    def test_ASL_absolute_X_c_z_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x0123] = 0x01

        self.ram[0x1000] = instructions.ASL.ABSOLUTE_X
        self.ram[0x1001] = 0x22
        self.ram[0x1002] = 0x01

        self.ram[0x1003] = instructions.ASL.ABSOLUTE_X
        self.ram[0x1004] = 0x22
        self.ram[0x1005] = 0x01

        self.run_clocks(7)
        assert_that(self.ram[0x0123]).is_equal_to(0x02)
        assert_that(self.cpu).has_PC(0x1003).has_Z(False).has_C(False).has_N(False)

        self.run_clocks(7)
        assert_that(self.ram[0x0123]).is_equal_to(0x04)
        assert_that(self.cpu).has_PC(0x1006).has_Z(False).has_C(False).has_N(False)
        self._assert_absolute_X_does_not_change_anything_else()

    def test_ASL_absolute_X_c_true_z_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x0123] = 0x82

        self.ram[0x1000] = instructions.ASL.ABSOLUTE_X
        self.ram[0x1001] = 0x22
        self.ram[0x1002] = 0x01

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_Z(False).has_C(True).has_N(False)
        assert_that(self.ram[0x0123]).is_equal_to(0x04)
        self._assert_absolute_X_does_not_change_anything_else()

    def test_ASL_absolute_X_c_z_true_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x0123] = 0x80

        self.ram[0x1000] = instructions.ASL.ABSOLUTE_X
        self.ram[0x1001] = 0x22
        self.ram[0x1002] = 0x01

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_Z(True).has_C(True).has_N(False)
        assert_that(self.ram[0x0123]).is_equal_to(0x00)
        self._assert_absolute_X_does_not_change_anything_else()

    def test_ASL_absolute_c_n_true_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x0123] = 0xC0

        self.ram[0x1000] = instructions.ASL.ABSOLUTE_X
        self.ram[0x1001] = 0x22
        self.ram[0x1002] = 0x01

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_Z(False).has_C(True).has_N(True)
        assert_that(self.ram[0x0123]).is_equal_to(0x80)
        self._assert_absolute_X_does_not_change_anything_else()
