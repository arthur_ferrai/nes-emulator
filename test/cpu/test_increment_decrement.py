from assertpy import assert_that

from src.cpu import instructions
from test.cpu.utils import CpuTestUtils


class TestCpuINCInstructions(CpuTestUtils):
    def test_INC_zero_page_z_true_n_false(self):
        self.ram[0x1000] = instructions.INC.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0xFF

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0x0001]).is_equal_to(0x00)

    def test_INC_zero_page_n_true_z_false(self):
        self.ram[0x1000] = instructions.INC.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x7F

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0x0001]).is_equal_to(0x80)

    def test_INC_zero_page_n_and_z_false(self):
        self.ram[0x1000] = instructions.INC.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0x0001]).is_equal_to(0x02)

    def test_INC_zero_page_X_z_true_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.INC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0002] = 0xFF

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0001]).is_equal_to(0x00)

    def test_INC_zero_page_X_n_true_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.INC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0002] = 0x7F

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0002]).is_equal_to(0x80)

    def test_INC_zero_page_X_n_and_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.INC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0002] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0002]).is_equal_to(0x02)

    def test_INC_zero_page_X_boundary_cross(self):
        self.cpu.X = 0xFF
        self.ram[0x1000] = instructions.INC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0000] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_X(0xFF)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0000]).is_equal_to(0x02)

    def test_INC_absolute_z_true_n_false(self):
        self.ram[0x1000] = instructions.INC.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0xFF

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0xABCD]).is_equal_to(0x00)

    def test_INC_absolute_n_true_z_false(self):
        self.ram[0x1000] = instructions.INC.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x7F

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0xABCD]).is_equal_to(0x80)

    def test_INC_absolute_n_and_z_false(self):
        self.ram[0x1000] = instructions.INC.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0xABCD]).is_equal_to(0x02)

    def test_INC_absolute_X_z_true_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.INC.ABSOLUTE_X
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCE] = 0xFF

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x01).has_N(False).has_Z(True)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0xABCE]).is_equal_to(0x00)

    def test_INC_absolute_X_n_true_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.INC.ABSOLUTE_X
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCE] = 0x7F

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x01).has_N(True).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0xABCE]).is_equal_to(0x80)

    def test_INC_absolute_X_n_and_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.INC.ABSOLUTE_X
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCE] = 0x01

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x01).has_N(False).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0xABCE]).is_equal_to(0x02)


class TestCpuINXInstructions(CpuTestUtils):
    def test_INX_implied_z_true_n_false(self):
        self.cpu.X = 0xFF
        self.ram[0x1000] = instructions.INX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(0x00).has_N(False).has_Z(True)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_INX_implied_n_true_z_false(self):
        self.cpu.X = 0x7F
        self.ram[0x1000] = instructions.INX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(0x80).has_N(True).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_INX_implied_n_false_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.INX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(0x02).has_N(False).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))


class TestCpuINYInstructions(CpuTestUtils):
    def test_INY_implied_z_true_n_false(self):
        self.cpu.Y = 0xFF
        self.ram[0x1000] = instructions.INY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Y(0x00).has_N(False).has_Z(True)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_INY_implied_n_true_z_false(self):
        self.cpu.Y = 0x7F
        self.ram[0x1000] = instructions.INY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Y(0x80).has_N(True).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_INY_implied_n_false_z_false(self):
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.INY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Y(0x02).has_N(False).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "Y"))


class TestCpuDECInstructions(CpuTestUtils):
    def test_DEC_zero_page_z_true_n_false(self):
        self.ram[0x1000] = instructions.DEC.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x01

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0x0001]).is_equal_to(0x00)

    def test_DEC_zero_page_n_true_z_false(self):
        self.ram[0x1000] = instructions.DEC.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x81

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0x0001]).is_equal_to(0x80)

    def test_DEC_zero_page_n_and_z_false(self):
        self.ram[0x1000] = instructions.DEC.ZERO_PAGE
        self.ram[0x1001] = 0x01
        self.ram[0x0001] = 0x03

        self.run_clocks(5)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0x0001]).is_equal_to(0x02)

    def test_DEC_zero_page_X_z_true_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.DEC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0002] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(True).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0001]).is_equal_to(0x00)

    def test_DEC_zero_page_X_n_true_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.DEC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0002] = 0x81

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(True).has_Z(False).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0002]).is_equal_to(0x80)

    def test_DEC_zero_page_X_n_and_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.DEC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0002] = 0x03

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_X(0x01)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0002]).is_equal_to(0x02)

    def test_DEC_zero_page_X_boundary_cross(self):
        self.cpu.X = 0xFF
        self.ram[0x1000] = instructions.DEC.ZERO_PAGE_X
        self.ram[0x1001] = 0x01
        self.ram[0x0000] = 0x03

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1002).has_N(False).has_Z(False).has_X(0xFF)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0x0000]).is_equal_to(0x02)

    def test_DEC_absolute_z_true_n_false(self):
        self.ram[0x1000] = instructions.DEC.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x01

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(True).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0xABCD]).is_equal_to(0x00)

    def test_DEC_absolute_n_true_z_false(self):
        self.ram[0x1000] = instructions.DEC.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x81

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_N(True).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0xABCD]).is_equal_to(0x80)

    def test_DEC_absolute_n_and_z_false(self):
        self.ram[0x1000] = instructions.DEC.ABSOLUTE
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCD] = 0x03

        self.run_clocks(6)
        assert_that(self.cpu).has_PC(0x1003).has_N(False).has_Z(False).has_no_changes(everything_except=("PC", "P"))
        assert_that(self.ram[0xABCD]).is_equal_to(0x02)

    def test_DEC_absolute_X_z_true_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.DEC.ABSOLUTE_X
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCE] = 0x01

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x01).has_N(False).has_Z(True)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0xABCE]).is_equal_to(0x00)

    def test_DEC_absolute_X_n_true_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.DEC.ABSOLUTE_X
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCE] = 0x81

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x01).has_N(True).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0xABCE]).is_equal_to(0x80)

    def test_DEC_absolute_X_n_and_z_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.DEC.ABSOLUTE_X
        self.ram[0x1001] = 0xCD
        self.ram[0x1002] = 0xAB
        self.ram[0xABCE] = 0x03

        self.run_clocks(7)
        assert_that(self.cpu).has_PC(0x1003).has_X(0x01).has_N(False).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))
        assert_that(self.ram[0xABCE]).is_equal_to(0x02)


class TestCpuDEXInstructions(CpuTestUtils):
    def test_DEX_implied_z_true_n_false(self):
        self.cpu.X = 0x01
        self.ram[0x1000] = instructions.DEX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(0x00).has_N(False).has_Z(True)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_DEX_implied_n_true_z_false(self):
        self.cpu.X = 0x81
        self.ram[0x1000] = instructions.DEX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(0x80).has_N(True).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))

    def test_DEX_implied_n_false_z_false(self):
        self.cpu.X = 0x03
        self.ram[0x1000] = instructions.DEX.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_X(0x02).has_N(False).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "X"))


class TestCpuDEYInstructions(CpuTestUtils):
    def test_DEY_implied_z_true_n_false(self):
        self.cpu.Y = 0x01
        self.ram[0x1000] = instructions.DEY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Y(0x00).has_N(False).has_Z(True)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_DEY_implied_n_true_z_false(self):
        self.cpu.Y = 0x81
        self.ram[0x1000] = instructions.DEY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Y(0x80).has_N(True).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "Y"))

    def test_DEY_implied_n_false_z_false(self):
        self.cpu.Y = 0x03
        self.ram[0x1000] = instructions.DEY.IMPLIED

        self.run_clocks(2)
        assert_that(self.cpu).has_PC(0x1001).has_Y(0x02).has_N(False).has_Z(False)\
            .has_no_changes(everything_except=("PC", "P", "Y"))
